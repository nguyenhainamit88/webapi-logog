const https = require('https');
const crypto = require('crypto');
const config = require('../configs/app').configs();
const region = config.region;
const endpoint = config.es.endpoint;
const esIndex = config.es.esIndex;
const service = config.es.service;

const transform = (source) => {
    let action = { "index": {} };
    
    action.index._index = esIndex;
    
    return [ 
        JSON.stringify(action), 
        JSON.stringify(source),
    ].join('\n') + '\n';
}

function post(body, callback) {
    let requestParams = buildRequest(endpoint, body);
    let request = https.request(requestParams, function(response) {
        let responseBody = '';
        response.on('data', function(chunk) {
            responseBody += chunk;
        });

        response.on('end', function() {
            let info = JSON.parse(responseBody);
            let failedItems;
            let success;
            let error;
            
            if (response.statusCode >= 200 && response.statusCode < 299) {
                failedItems = info.items.filter(function(x) {
                    return x.index.status >= 300;
                });

                success = { 
                    "attemptedItems": info.items.length,
                    "successfulItems": info.items.length - failedItems.length,
                    "failedItems": failedItems.length
                };
            }

            if (response.statusCode !== 200 || info.errors === true) {
                // prevents logging of failed entries, but allows logging 
                // of other errors such as access restrictions
                delete info.items;
                error = {
                    statusCode: response.statusCode,
                    responseBody: info
                };
            }

            callback(error, success, response.statusCode, failedItems);
        });
    }).on('error', function(e) {
        callback(e);
    });
    request.end(requestParams.body);
}

function buildRequest(endpoint, body) {
    let datetime = (new Date()).toISOString().replace(/[:\-]|\.\d{3}/g, '');
    let date = datetime.substr(0, 8);
    let kDate = hmac('AWS4' + process.env.AWS_SECRET_ACCESS_KEY, date);
    let kRegion = hmac(kDate, region);
    let kService = hmac(kRegion, service);
    let kSigning = hmac(kService, 'aws4_request');
    
    let request = {
        host: endpoint,
        method: 'POST',
        path: '/_bulk',
        body: body,
        headers: { 
            'Content-Type': 'application/json',
            'Host': endpoint,
            'Content-Length': Buffer.byteLength(body),
            'X-Amz-Security-Token': process.env.AWS_SESSION_TOKEN,
            'X-Amz-Date': datetime
        }
    };

    let canonicalHeaders = Object.keys(request.headers)
        .sort(function(a, b) { 
            return a.toLowerCase() < b.toLowerCase() ? -1 : 1; 
        })
        .map(function(k) { 
            return k.toLowerCase() + ':' + request.headers[k]; 
        })
        .join('\n');

    let signedHeaders = Object.keys(request.headers)
        .map(function(k) { 
            return k.toLowerCase();
        })
        .sort()
        .join(';');

    let canonicalString = [
        request.method,
        request.path, '',
        canonicalHeaders, '',
        signedHeaders,
        hash(request.body, 'hex'),
    ].join('\n');

    let credentialString = [ 
        date, 
        region, 
        service, 
        'aws4_request' 
    ].join('/');

    let stringToSign = [
        'AWS4-HMAC-SHA256',
        datetime,
        credentialString,
        hash(canonicalString, 'hex')
    ] .join('\n');

    request.headers.Authorization = [
        'AWS4-HMAC-SHA256 Credential=' + process.env.AWS_ACCESS_KEY_ID + '/' + credentialString,
        'SignedHeaders=' + signedHeaders,
        'Signature=' + hmac(kSigning, stringToSign, 'hex')
    ].join(', ');

    return request;
}

function hmac(key, str, encoding) {
    return crypto.createHmac('sha256', key)
        .update(str, 'utf8')
        .digest(encoding);
}

function hash(str, encoding) {
    return crypto.createHash('sha256')
        .update(str, 'utf8')
        .digest(encoding);
}

module.exports = {
    transform,
    post
}
