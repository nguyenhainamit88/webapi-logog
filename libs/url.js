// libs/url.js

function service(requestUrl) {
    let service = '';
    // grab service
    if (/propertydetails/i.test(requestUrl)) {
        service = "propertydetails";
    } else if (/(searchproperties|searchresalexml)/i.test(requestUrl)) {
        service = "searchproperties";
    } else if (/(searchrentalxml)/i.test(requestUrl)) {
        service = "searchrental";
    } else if (/searchlocations/i.test(requestUrl)) {
        service = "searchlocations";
    } else if (/searchpropertytypes/i.test(requestUrl)) {
        service = "searchpropertytypes";
    } else if (/searchfeatures/i.test(requestUrl)) {
        service = "searchfeatures";
    } else if (/searchlanguagesxml/i.test(requestUrl)) {
        service = "searchlanguages";
    } else if (/bookingcalendar/i.test(requestUrl)) {
        service = "bookingcalendar";
    } else if (/registerleadpropextra/i.test(requestUrl)) {
        service = "registerleadpropextra";
    } else if (/registerleadwebkit/i.test(requestUrl)) {
        service = "registerleadwebkit";
    } else if (/registerlead/i.test(requestUrl)) {
        service = "registerlead";
    } else {
    }
    return service;
}

// grab version
function version(requestUrl) {
    let rx = /\/([Vv]?\d{1}[\.\-]?\d?)\//;
    let cap = rx.exec(requestUrl) || ["", ""];
    let v = "";
    if (cap[1] !== "") {
        v = cap[1].replace("-", ".");
        if (v[0] !== "v") {
        v = "v" + v;
        }
        if (v.length === 2) {
            if (v === 'v5') {
                v += ".3" // lastest of v5
            } else {
                v += ".0";
            }
        }
    }
    return v;
}

module.exports = {
    version,
    service
}