const es = require('./libs/es');
const urlLibs = require('./libs/url');

exports.handler = (event, context, callback) => {
    
    const request = event.Records[0].cf.request;
    const response = event.Records[0].cf.response;
    const headers = response.headers;
    const queryString = request.querystring;
    /*global URLSearchParams*/
    const urlParams = new URLSearchParams(queryString);
    
    const clientIp = request.clientIp;
    const host = request.headers["host"][0].value;
    // const userAgent = request.headers["user-agent"][0].value;
    const uri = request.uri.toLocaleLowerCase();
    const p1 = urlParams.get('p1');
    const p2 = urlParams.get('p2');
    const pApiId = urlParams.get('P_ApiId');
    // const pCountry = urlParams.get('p_country');
    // const pIgnorehash = urlParams.get('P_ignorehash');
    const date = headers.date[0].value;
    const versionAPI = urlLibs.version(uri);
    const serviceAPI = urlLibs.service(uri);

    let data = es.transform({
        clientIp,
        host,
        // userAgent,
        uri,
        p1,
        p2,
        pApiId,
        // pCountry, 
        // pIgnorehash,
        date, 
        versionAPI,
        serviceAPI
    });
    
    // post documents to the Amazon Elasticsearch Service
    es.post(data, function(error, success, statusCode, failedItems) {
        if (error) {
            context.fail(JSON.stringify(error));
        } else {
            context.succeed(JSON.stringify(success));
        }
    });
    
    callback(null, response);
};