// events/cloudfront.js

const config = require("../configs/app").configs();
const versions = require("../configs/versions");

module.exports.webapilog = () => {
    const arrVersions = versions();
    let preExistingCloudFront = [];

    arrVersions.map(function (pathPattern) {
        preExistingCloudFront.push(
            {
                preExistingCloudFront: {
                    distributionId: config.cloudfront.distributionId,
                    eventType: config.cloudfront.eventType,
                    pathPattern: pathPattern,
                    includeBody: config.cloudfront.includeBody
                }
            }
        );

        });

    return preExistingCloudFront;
}