## 1. Install Provider Plugin & Service Dependencies
`npm install` in this directory to download the modules from `package.json`.

## 2. Deploy
`serverless deploy` or `sls deploy`. `sls` is shorthand for the Serverless CLI command

## 3. Invoke deployed function
`serverless invoke --function webapilog` or `serverless invoke -f webapilog`

`-f` is shorthand for `--function`
