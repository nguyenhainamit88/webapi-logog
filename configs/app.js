// app.js

module.exports.configs = () => {
    /**
     * Dev environment
     */
    return {
        region: "us-east-1",
        es: {
            endpoint: "search-webapi-log-prod-xxx.us-east-1.es.amazonaws.com",
            esIndex: "webapi-access-log-dev",
            service: "es",
        },
        lambda: {
            timeout: "5",
            memorySize: "128",
        },
        cloudfront: {
            distributionId: "xxx",
            eventType: "viewer-response", // viewer-request, origin-request, origin-response, viewer-response
            includeBody: false
        }
    };

    /**
     * Prod environment
     */
    // return {
    //     region: "us-east-1",
    //     es: {
    //         endpoint: "search-webapi-log-prod-xxx.us-east-1.es.amazonaws.com",
    //         esIndex: "webapi-access-log-prod",
    //         service: "es",
    //     },
    //     lambda: {
    //         timeout: "5",
    //         memorySize: "128",
    //     },
    //     cloudfront: {
    //         distributionId: "xxx",
    //         eventType: "viewer-response", // viewer-request, origin-request, origin-response, viewer-response
    //         includeBody: false
    //     }
    // };
}
